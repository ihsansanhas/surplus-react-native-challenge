import { FlatList, Image, StyleSheet, Text, View } from "react-native";
import Color from "../../../utils/color";
import Rainy from "../../../assets/icons/cloud_drizzel_rain_icon.png";
import CloudyNight from "../../../assets/icons/clouds_night_storm_icon.png";
import CloudyDay from "../../../assets/icons/cloud_clouds_cloudy_icon.png";
import ShowerDay from "../../../assets/icons/rain_shower_storm_icon.png";
import ShowerNight from "../../../assets/icons/clouds_moon_night_rain_icon.png";
import RainNight from "../../../assets/icons/night_rain_storm_icon.png";
import RainDay from "../../../assets/icons/clouds_rain_storm_thunder_icon.png";
import ClearDay from "../../../assets/icons/clouds_sun_sunny_icon.png";
import ClearNight from "../../../assets/icons/clouds_cloudy_moon_icon.png";

const WeatherCard = ({ data }) => {
  const getSource = (weather) => {
    if (weather === "rain") {
      return Rainy;
    } else if (
      weather === "cloudynight" ||
      weather === "pcloudynight" ||
      weather === "mcloudynight"
    ) {
      return CloudyNight;
    } else if (
      weather === "cloudyday" ||
      weather === "mcloudyday" ||
      weather === "pcloudyday" ||
      weather === "pcloudy"
    ) {
      return CloudyDay;
    } else if (
      weather === "ishower" ||
      weather === "ishowerday" ||
      weather === "oshowerday"
    ) {
      return ShowerDay;
    } else if (weather === "ishowernight") {
      return ShowerNight;
    } else if (weather === "lightrainnight") {
      return RainNight;
    } else if (weather === "lightrain" || weather === "lightrainday") {
      return RainDay;
    } else if (weather === "clear" || weather === "clearday") {
      return ClearDay;
    } else if (weather === "clearnight") {
      return ClearNight;
    }
    return null;
  };
  return (
    <FlatList
      data={data}
      numColumns={2}
      keyExtractor={(item) => item.id}
      contentContainerStyle={{ justifyContent: "space-around", gap: 16 }}
      columnWrapperStyle={{ flexShrink: 1, gap: 16 }}
      showsVerticalScrollIndicator={false}
      renderItem={(itemData) => (
        <View style={styles.weatherItem}>
          <Image
            style={styles.icon}
            source={getSource(itemData.item.weather)}
          />
          <Text>{itemData.item.weather}</Text>
        </View>
      )}
    />
  );
};

export default WeatherCard;

const styles = StyleSheet.create({
  weatherItem: {
    backgroundColor: "white",
    padding: 16,
    elevation: 3,
    shadowColor: Color.secondary,
    shadowOpacity: 0.5,
    borderRadius: 8,
    alignItems: "center",
  },
  icon: {
    width: 150,
    height: 150,
    resizeMode: "contain",
  },
});
