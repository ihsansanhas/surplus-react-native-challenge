import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Color from "../../../utils/color";

const ButtonText = ({ onPress, title, description }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.description}>{description}</Text>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ButtonText;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  description: {
    textAlign: "center",
    color: Color.secondary,
    marginRight: 6,
  },
  title: {
    color: Color.primary,
    fontWeight: 600,
  },
});
