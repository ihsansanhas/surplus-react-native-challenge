import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import Color from "../../../utils/color";

const Button = ({
  title,
  onPress,
  bgColor,
  txtColor,
  disabled,
  btnStyle,
  textStyle,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onPress}
      style={[
        styles.button,
        {
          backgroundColor: disabled ? Color.disabled : bgColor || Color.primary,
          borderColor: disabled ? Color.disabled : Color.primary,
        },
        btnStyle,
      ]}
      disabled={disabled}
    >
      <Text
        style={[
          styles.title,
          { color: disabled ? Color.secondary : txtColor || "white" },
          textStyle,
        ]}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  button: {
    padding: 14,
    borderRadius: 50,
    borderColor: Color.primary,
    borderWidth: 1,
  },
  title: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: 500,
  },
});
