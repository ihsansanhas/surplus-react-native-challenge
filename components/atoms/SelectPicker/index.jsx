import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { Picker } from "@react-native-picker/picker";
import Color from "../../../utils/color";

const SelectPicker = ({
  options = [],
  selectedValue,
  label,
  onValueChange,
}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.picker}>
        <Picker selectedValue={selectedValue} onValueChange={onValueChange}>
          {options.map((option) => (
            <Picker.Item label={option.label} value={option.value} />
          ))}
        </Picker>
      </View>
    </View>
  );
};

export default SelectPicker;

const styles = StyleSheet.create({
  label: {
    fontSize: 14,
    marginBottom: 6,
    color: Color.secondary,
  },
  picker: {
    backgroundColor: "white",
    borderRadius: 8,
  },
});
