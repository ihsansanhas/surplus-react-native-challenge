import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import Color from "../../../utils/color";
import { useState } from "react";
import { Feather } from "@expo/vector-icons";

const Input = ({
  label,
  placeholder,
  onFocus,
  onBlur,
  type,
  onChangeText,
  value,
  isError,
  errorText,
}) => {
  const [isFocus, setIsFocus] = useState(false);
  const [isSecure, setIsSecure] = useState(true);
  const isPasswordInput = type === "password";

  const handleFocus = () => {
    setIsFocus(true);
    onFocus && onFocus();
  };

  const handleBlur = () => {
    setIsFocus(false);
    onBlur && onBlur();
  };

  const handleShowHidePassword = () => {
    setIsSecure(!isSecure);
  };

  return (
    <View>
      <Text
        style={[
          styles.label,
          {
            color: isError
              ? Color.danger
              : isFocus
              ? Color.primary
              : Color.secondary,
          },
        ]}
      >
        {label}
      </Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={[
            styles.input,
            {
              borderColor: isError
                ? Color.danger
                : isFocus
                ? Color.primary
                : Color.secondary,
            },
          ]}
          placeholder={placeholder}
          onFocus={handleFocus}
          onBlur={handleBlur}
          secureTextEntry={isSecure && isPasswordInput}
          onChangeText={onChangeText}
          value={value}
        />
        {isPasswordInput && (
          <Pressable style={styles.secureIcon} onPress={handleShowHidePassword}>
            {isSecure ? (
              <Feather name="eye-off" size={24} color={Color.secondary} />
            ) : (
              <Feather name="eye" size={24} color={Color.secondary} />
            )}
          </Pressable>
        )}
      </View>
      {errorText && <Text style={styles.errorText}>{errorText}</Text>}
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  label: {
    fontSize: 16,
    marginBottom: 12,
    fontWeight: 500,
    color: Color.secondary,
  },
  inputContainer: {
    flexDirection: "row",
    position: "relative",
  },
  input: {
    borderWidth: 1,
    borderColor: Color.secondary,
    paddingHorizontal: 16,
    paddingVertical: 14,
    borderRadius: 16,
    width: "100%",
  },
  secureIcon: {
    position: "absolute",
    right: 16,
    top: 14,
    padding: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  errorText: {
    color: Color.danger,
    marginTop: 3,
    fontSize: 11,
  },
});
