import { useContext, useEffect, useState } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SplashScreen from "../screens/SplashScreen";
import Welcome from "../screens/Welcome";
import SignUp from "../screens/SignUp";
import SignIn from "../screens/SignIn";
import Weather from "../screens/Weather";
import { AuthContext } from "../store/auth-context";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import BottomTab from "../components/organisms/BottomTab";
import Button from "../components/atoms/Button";
import { View } from "react-native";

const Stack = createNativeStackNavigator();
const Tabs = createBottomTabNavigator();

const MainApp = () => {
  const authCtx = useContext(AuthContext);
  return (
    <Tabs.Navigator
      initialRouteName="Weather"
      tabBar={(props) => <BottomTab {...props} />}
      screenOptions={{
        title: "Weather App",
        headerRight: () => (
          <View style={{ marginRight: 16 }}>
            <Button
              title="Logout"
              onPress={() => authCtx.logout()}
              btnStyle={{ paddingHorizontal: 10, paddingVertical: 4 }}
              textStyle={{ fontSize: 14 }}
            />
          </View>
        ),
      }}
    >
      <Tabs.Screen name="Weather" component={Weather} />
    </Tabs.Navigator>
  );
};

const StackNav = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Welcome"
        component={Welcome}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AuthStackNav = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const Navigation = () => {
  const authCtx = useContext(AuthContext);
  const [isTryLogin, setIsTryLogin] = useState(true);

  const fetchToken = async () => {
    const storeToken = await AsyncStorage.getItem("token");

    if (storeToken) {
      authCtx.authenticate(storeToken);
    }

    setIsTryLogin(false);
  };

  useEffect(() => {
    fetchToken();
  }, []);

  if (isTryLogin) {
    return null;
  }

  return (
    <NavigationContainer>
      {!authCtx.isAuthenticated && <StackNav />}
      {authCtx.isAuthenticated && <AuthStackNav />}
    </NavigationContainer>
  );
};

export default Navigation;
