import Navigation from "./navigations";
import AuthContextProvider from "./store/auth-context";

export default function App() {
  return (
    <AuthContextProvider>
      <Navigation />
    </AuthContextProvider>
  )
}
