export const initForm = {
  email: "",
  password: "",
  passwordConf: "",
}

export const initError = {
  email: {
    value: false,
    text: "",
  },
  password: {
    value: false,
    text: "",
  },
  passwordConf: {
    value: false,
    text: "",
  },
}