import { useContext, useState } from "react";
import { Image, StyleSheet, View } from "react-native";
import Logo from "../../assets/images/logo.png";
import Color from "../../utils/color";
import Button from "../../components/atoms/Button";
import Input from "../../components/atoms/Input";
import ButtonText from "../../components/molecules/ButtonText";
import { isEmail, validateAuthForm } from "../../utils/validation";
import { setFormState } from "../../utils/input";
import { fetchRegister } from "../../utils/auth-http";
import { AuthContext } from "../../store/auth-context";
import { initError, initForm } from "./constant";

const SignUp = ({ navigation }) => {
  const authCtx = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(initError);
  const [form, setForm] = useState(initForm);

  const handleInputForm = (name, value) => {
    validateAuthForm({ name, value, setError, form });
    setFormState({ setState: setForm, name, value });
  };

  const isAllowSubmit =
    form.email !== "" &&
    form.password !== "" &&
    form.passwordConf !== "" &&
    error.password.text === "" &&
    error.email.text === "" &&
    error.passwordConf.text === "" &&
    isEmail(form.email) &&
    form.password === form.passwordConf;

  const handleRegister = async () => {
    try {
      setLoading(true);
      const token = await fetchRegister(form.email, form.password);
      setLoading(false);
      authCtx.authenticate(token);
      setForm(initForm);
      setError(initError);
    } catch (error) {
      setLoading(false);
      throw new Error(error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.hero}>
        <Image style={styles.logo} source={Logo} />
      </View>
      <View style={styles.content}>
        <View style={styles.form}>
          <Input
            label="E-mail"
            placeholder="Masukkan email"
            value={form.email}
            isError={error.email.value}
            errorText={error.email.text}
            onChangeText={(value) => handleInputForm("email", value)}
          />
          <View style={{ height: 24 }} />
          <Input
            label="Password"
            placeholder="Masukkan password"
            type="password"
            value={form.password}
            isError={error.password.value}
            errorText={error.password.text}
            onChangeText={(value) => handleInputForm("password", value)}
          />
          <View style={{ height: 24 }} />
          <Input
            label="Konfirmasi Password"
            placeholder="Masukkan konfirmasi password"
            type="password"
            value={form.passwordConf}
            isError={error.passwordConf.value}
            errorText={error.passwordConf.text}
            onChangeText={(value) => handleInputForm("passwordConf", value)}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title={loading ? "Proses..." : "Daftar"}
            onPress={handleRegister}
            disabled={!isAllowSubmit || loading}
          />
          <View style={styles.btnTextContaier}>
            <ButtonText
              description="Sudah Punya Akun ?"
              title="Login"
              onPress={() => navigation.navigate("SignIn")}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  hero: {
    backgroundColor: Color.primary,
    justifyContent: "center",
    alignItems: "center",
    height: 250,
  },
  logo: {
    width: 140,
    resizeMode: "contain",
  },
  content: {
    backgroundColor: "white",
    flex: 1,
    marginTop: -40,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingHorizontal: 24,
    paddingVertical: 32,
    elevation: 4,
    justifyContent: "space-between",
  },
  buttonContainer: {
    marginTop: 16,
    backgroundColor: "white",
  },
  btnTextContaier: {
    marginTop: 16,
    alignItems: "center",
  },
});
