import { Image, StyleSheet, Text, View } from "react-native";
import Logo from "../../assets/images/logo.png";
import Color from "../../utils/color";
import Button from "../../components/atoms/Button";

const Welcome = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.hero}>
        <Image style={styles.logo} source={Logo} />
      </View>
      <View style={styles.content}>
        <View>
          <Text style={styles.title}>Selamat datang di WeatherApp</Text>
          <Text style={styles.subtitle}>
            Ini adalah Aplikasi untuk melihat kondisi cuaca saat ini.
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Daftar"
            onPress={() => navigation.navigate("SignUp")}
          />
          <View style={{ height: 24 }} />
          <Button
            title="Sudah Punya Akun? Masuk"
            bgColor="white"
            txtColor={Color.primary}
            onPress={() => navigation.navigate("SignIn")}
          />
        </View>
      </View>
    </View>
  );
};

export default Welcome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  hero: {
    backgroundColor: Color.primary,
    justifyContent: "center",
    alignItems: "center",
    height: 500,
  },
  logo: {
    width: 200,
    resizeMode: "contain",
  },
  content: {
    backgroundColor: "white",
    flex: 1,
    marginTop: -40,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingHorizontal: 24,
    paddingVertical: 32,
    elevation: 4,
  },
  title: {
    fontSize: 18,
    fontWeight: 600,
    textAlign: "center",
  },
  subtitle: {
    textAlign: "center",
    marginTop: 6,
    paddingHorizontal: 48,
    fontSize: 12,
    color: Color.secondary,
  },
  buttonContainer: {
    marginTop: 48,
  },
});
