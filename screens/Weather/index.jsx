import { useEffect, useState } from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import * as Location from "expo-location";
import Color from "../../utils/color";
import { fetchWeather } from "../../utils/weather-http";
import WeatherCard from "../../components/molecules/WeatherCard";
import SelectPicker from "../../components/atoms/SelectPicker";

const Weather = () => {
  const [loading, setLoading] = useState(false);
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [dataWeather, setDataWeather] = useState({});
  const [product, setProduct] = useState("civil");

  const getCurrentLocation = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      setErrorMsg("Permission to access location was denied");
      return;
    }

    let location = await Location.getCurrentPositionAsync({});
    setLatitude(location.coords.latitude);
    setLongitude(location.coords.longitude);
  };

  useEffect(() => {
    getCurrentLocation();
  }, []);

  useEffect(() => {
    async function getWeather() {
      setLoading(true);
      const data = await fetchWeather({
        latitude,
        longitude,
        product,
      });
      setLoading(false);
      const newDataSeries =
        data &&
        data?.dataseries?.map((item, i) => ({
          id: i,
          ...item,
        }));
      setDataWeather({
        ...data,
        dataseries: newDataSeries,
      });
    }
    getWeather();
  }, [latitude, longitude, product]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.picker}>
          <SelectPicker
            selectedValue={product}
            label="Pilih Produk"
            onValueChange={(itemValue) => setProduct(itemValue)}
            options={[
              {
                label: "Civil",
                value: "civil",
              },
              {
                label: "Civil Light",
                value: "civillight",
              },
              {
                label: "Two",
                value: "two",
              },
            ]}
          />
        </View>
        <View style={{ height: 24 }} />
        <Text style={styles.title}>{dataWeather?.product}</Text>
      </View>
      {loading ? (
        <ActivityIndicator size={50} color={Color.primary} />
      ) : (
        <WeatherCard data={dataWeather?.dataseries} />
      )}
    </View>
  );
};

export default Weather;

const styles = StyleSheet.create({
  header: {
    marginBottom: 16,
  },
  title: {
    fontSize: 24,
    textTransform: "capitalize",
    fontWeight: 500,
  },
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 16,
    backgroundColor: Color.disabled,
  },
});
