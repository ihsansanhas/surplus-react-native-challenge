import { useEffect, useRef } from "react";
import { Animated, Image, StyleSheet, Text, View } from "react-native";
import Logo from "../../assets/images/logo.png";
import Color from "../../utils/color";

const SplashScreen = ({ navigation }) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 2000,
      useNativeDriver: false,
    }).start();
  };

  useEffect(() => {
    fadeIn();
    setTimeout(() => {
      navigation.replace("Welcome");
    }, 3000);
  }, []);
  return (
    <View style={styles.container}>
      <Animated.View>
        <View style={styles.logoContainer}>
          <Image source={Logo} style={styles.logo} />
          <Text style={styles.title}>WeatherApp</Text>
        </View>
      </Animated.View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Color.primary,
  },
  logo: {
    width: 200,
    height: 150,
    resizeMode: "contain",
  },
  logoContainer: {
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    marginTop: -10,
    fontWeight: 600,
  },
});
