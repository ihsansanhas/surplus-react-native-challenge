import axios from "axios"

const API_KEY = 'AIzaSyDbOBI-kkFxhKf3VeeDXT7EDFHa0eAoySU' // better to save in ENV
const BASE_AUTH_URL = 'https://identitytoolkit.googleapis.com/v1/accounts'

export const authenticate = async (type, email, password) => {
  const url = `${BASE_AUTH_URL}:${type}?key=${API_KEY}`

  const response = await axios.post(url, {
    email,
    password,
    returnSecureToken: true,
  })

  return response.data.idToken
}

export const fetchRegister = (email, password) => {
  return authenticate('signUp', email, password)
}

export const fetchLogin = (email, password) => {
  return authenticate('signInWithPassword', email, password)
}