import { isEmail } from "../validation";

describe('Test Validation Utils', () => {
  test('should return true when input valid email format', () => {
    expect(isEmail('ihksansanhas@gmail.com')).toBeTruthy()
  })
  test('should return false when input invalid email format', () => {
    expect(isEmail('ihksansanhas@gmailcom')).toBeFalsy()
  })

})