const Color = {
  primary: '#47b4d8',
  secondary: '#8C8C8C',
  danger: '#d84747',
  disabled: '#efefef'
}

export default Color;