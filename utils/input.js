export const setFormState = ({ setState, name, value }) => {
  setState((currentState) => ({
    ...currentState,
    [name]: value,
  }));
};