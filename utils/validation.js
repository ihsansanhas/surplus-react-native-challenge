import { setFormState } from "./input";

export const isEmail = (value) => {
  const emailRgx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  return emailRgx.test(value)
}

export const validateAuthForm = ({name, value, setError, form}) => {
  const resetValue = (name) => {
    setFormState({
      setState: setError,
      name,
      value: {
        value: false, 
        text: ''
      },
    });
  }
  if (name === "email") {
    if (!isEmail(value)) {
      setFormState({
        setState: setError,
        name: "email",
        value: {
          value: true,
          text: "Format Email Salah",
        },
      });
    } else {
      resetValue("email")
    }
  }
  if (name === "password") {
    if (value.length < 6) {
      setFormState({
        setState: setError,
        name: "password",
        value: {
          value: true,
          text: "Password Minimal 6 Karakter",
        },
      });
    } else {
      resetValue("password")
    }
  }
  if(name === 'passwordConf') {
    if(value !== form.password) {
      setFormState({
        setState: setError,
        name: "passwordConf",
        value: {
          value: true,
          text: "Konfirmasi Password Belum sesuai",
        },
      });
    } else {
      resetValue("passwordConf")
    }
  }
};