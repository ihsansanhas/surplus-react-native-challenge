import axios from "axios";

const WEATHER_API = 'http://www.7timer.info/bin/api.pl'

export const fetchWeather = async ({longitude, latitude, product}) => {
  const url = `${WEATHER_API}?lon=${longitude}&lat=${latitude}&product=${product}&output=json`;
  try {
    const res = await axios.get(url);
    return res.data;
  } catch (error) {
    return error;
  }
};